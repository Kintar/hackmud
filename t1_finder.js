function(context, args)
{
	var keys = [".access",".pub",".pub_info",".extern",".external",".p",".public",".info",".entry",".out"]
	var caller = context.caller;
	var l = #s.scripts.lib();
	
	var result = #s.scripts.fullsec();
	var response = "";

	for (var i = 0; i < result.length; i++) {
		for (var n = 0; n < keys.length; n++) {
			if (result[i].endsWith(keys[n])) {
				response = response + "kelvor.t1_harvester {t:#s." + result[i] + "}\n";
				break;
			}
		}
	}
	
	return { ok:true, msg:response };
}
