function(context, args)
{ 
	var order = ["script_space","script","lock"], prg = -1, i, h = [], m = [], l = #s.scripts.lib(),
				count = #s.sys.upgrades().length;
	
	function v(a) {
		return (order.indexOf(a.d.type) * 1000) 
			+  (4 - a.d.rarity * 100)
			+  (l.hash_code(a.d.name) % 10)
			+  (a.d.loaded ? 10 : 0);
	}
	
	if (args) {
		if (args.order) order = args.order;
		if (args.purge) prg = args.purge;
	}
		
	for (i = 0; i < count; i++) {
		h.push({idx:i, d:#s.sys.upgrades({info:i})});
	}
	
	// The idea here is to keep track of each change we make as we sort
	// the array in memory, then apply that list of changes to the 
	// sys.upgrades {reorder:{}} call.
	
	h.sort(function (a,b) {
		if (!b) return 0;
		return v(a) - v(b);
	});
	
	i = 0;
	
	l.each(h, function (k,v)
	{
		if (v.idx + i != k) {
			m.push({from:v.idx, to:k});
			l.each(h, function(k2,v2) {if (v2.idx >= k && v2.idx < v.idx) v2.idx++;});
		}
	});
	
	#s.sys.upgrades({reorder:m});
	
	h = [];
	for (i = 0; i < count; i++) {
		h.push({idx:i, d:#s.sys.upgrades({info:i})});
	}
	
	h.reverse();
	m = [];
	l.each(h, function(k,v) {
		if (v.d.rarity == 0 && v.d.loaded == false) {
			#s.sys.upgrades({confirm:true, destroy:v.idx});
		}
	});
	
	return { ok:true };
}
