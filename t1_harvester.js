function(context, args)
{
	var l = #s.scripts.lib(), key, cmd, opts = {}, m, r, b, a, ps = [], users = [], p, rt = "",
			start = new Date().getTime(), i, x, u;
	
	r = args.t.call({});
	m = /-- access directory with\s+(\w+):"(\w+)"/.exec(r);
	if (!m) {
		return { ok:false, msg:"Failed to find directory args\n" + r};
	}
	
	key = m[1];
	cmd = m[2];
	
	r = args.t.call().split("\n");
	r = r[r.length - 1].split("|");
	
	b = r[0].trim();
	a = r[1].trim();
	
	opts[key] = b;
	
	r = args.t.call(opts);
	
	ps = [];
	l.each([/date for (\w+)/,/project (\w+)/,/for (\w+) in your/,/developments on (\w+)/,/continues on (\w+)/], function (k,m)
	{
		l.each(r, function(k2, t)
		{
			var z = m.exec(t);
			if (z) {
				ps.push(z[1])
			}
		});
	});
	
	opts[key] = a;
	r = args.t.call(opts);
	p = /strategy (\w+)/.exec(r);
	if (!p) {
		return {ok:false, msg:"Can't find password"};
	}
	p = p[1];
	
	opts[key] = cmd;
	opts.password = p;
	opts.pass = p;
	opts.p = p;
	
	var timeout = false;
	
	for (i = 0; i < ps.length; i++) {
		var proj = ps[i];
		opts["project"] = proj;
		var n = args.t.call(opts);
		for (x = 0; x < n.length; x++) {
			u = n[x];
			if (!u.startsWith("<") && u.includes(".")) {
				if (#s.scripts.get_level({name:u}) == 4 && users.indexOf(u) == -1) users.push(u);
			} 
			if (new Date().getTime() - start > 4500) {
				timeout = true;
				break;
			}
		}
		
		if (timeout) break;
	}
	
	l.each(users, function(k,v)
	{
		rt += context.caller + ".t1_crack {t:#s." + v + "}\n";
	});
	
	return { ok:true, msg:rt + "\n\n"};
}
